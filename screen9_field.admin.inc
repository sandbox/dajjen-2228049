<?php
/**
 * @file
 * This file is for admin configuration page for screen9 field.
 */

/**
 * Internal function for form.
 */
function _screen9_config_page_form($form, &$form_state) {

  $form = array();

  // Form item for url to screen9 XML-RPC.
  $form['screen9_xmlrpc_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Screen9 XML-RPC Url'),
    '#default_value' => variable_get('screen9_xmlrpc_url'),
    '#required' => TRUE,
  );

  $form['screen9_xmlrpc_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Screen9 XML-RPC port'),
    '#default_value' => variable_get('screen9_xmlrpc_port'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#maxlength' => 4,
    '#size' => 4,
    '#required' => TRUE,
  );

  $form['screen9_customer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Screen9 Customer ID'),
    '#default_value' => variable_get('screen9_customer_id'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#maxlength' => 6,
    '#size' => 6,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validation function for form.
 */
function _screen9_config_page_form_validate($form, &$form_state) {

  // Check if there is any input in this field.
  if (!isset($form_state['values']['screen9_xmlrpc_url'])) {
    form_set_error($form['screen9_xmlrpc_url']['#title'], t('Missing!'));
  }

  // Check if input string is a valid absolut url.
  elseif (!valid_url($form_state['values']['screen9_xmlrpc_url'], TRUE) ) {
    form_set_error($form['screen9_xmlrpc_url']['#title'], t('Not a valid URL, must be a absolut URL including http://'));
  }
}

/**
 * Submit function for form.
 */
function _screen9_config_page_form_submit($form, &$form_state) {
  if (isset($form_state['values']['screen9_xmlrpc_url'])) {
    variable_set('screen9_xmlrpc_url', check_url($form_state['values']['screen9_xmlrpc_url']));
    variable_set('screen9_xmlrpc_port', check_plain($form_state['values']['screen9_xmlrpc_port']));
    variable_set('screen9_customer_id', check_plain($form_state['values']['screen9_customer_id']));
    drupal_set_message(t('Your configuration has been saved.'));
  }
}
