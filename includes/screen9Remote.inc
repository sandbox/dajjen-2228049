<?php

/**
 * @file
 * Screen9 class.
 */


class Screen9_base {
  private $common;

  public function __construct() {
    $this->common = array(
      'version' => '2.0',
      'custid' => variable_get('screen9_customer_id'),
      'refer' => (string) url('<front>', array('absolute' => TRUE)),
      'userip' => (string) ip_address(),
      'browser' => 'drupal6-api',
    );
  }

  public function getCommon () {
    return $this->common;
  }
}
/**
 * Screen9 class
 */
class Screen9 extends Screen9_base{

  public function __construct() {
    parent::__construct();
  }
}

